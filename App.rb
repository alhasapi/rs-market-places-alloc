require "./Actors"
require "./Utils"

def prelude
  db = Market::DB.new
  created = false
  ask_with_action(HAS_ACCT) {
    nom, prenom, password = login
    registered =  db.is_known?( {nom => password} )
    created = true
    return Market::Responsable.new( nom, prenom, password, db ) if registered
    puts BAD_PSWD unless registered
  }
  ask_with_action(NEW_ACCT) {
    Market::Responsable.new( *(login) + [db] )
  } unless created
end


def alloc? un_agent
  ask_with_action(ALLOC_MSG) {
    nom, _, frais = login("compte")
    un_agent.allocation_pour nom, frais
  }
end

def remove_alloc? un_agent
  ask_with_action(DELOC_MSG) {
    un_agent.afficher_deja_allouer
    id = ask?("Entrer l'id du hangard: ").to_i
    un_agent.supprimer id
  }
end

if __FILE__ == $0 then
  begin
    loop do
      # login a << Responsable >> or a user
      responsable = prelude
      # Printing non empty hangard
      responsable.afficher_hangard_vide
      # Demande d'allocation
      alloc? responsable
      # allocate the << Hangard >> selected.
      # or suppress an allocation.
      remove_alloc? responsable
      ask_with_action("Voulez-vous quiter? ") {
        raise Stop
      }
    end
  rescue
    puts "Au revoir!"
  end
end
