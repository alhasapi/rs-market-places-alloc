require "yaml"

module Market
  PASSWORDS = "ps_db.yaml"
  USERS_DB  = "users_db.yaml"
  HANGARDS  = "hangards.yaml"
  HANGARD_MAX = 10

  class DB
    attr_reader :user_db, :pss_db, :hang_db
    def initialize dbs=[USERS_DB, PASSWORDS, HANGARDS]
      loader = ->( cnf ) { YAML::load File::read( cnf )}
      @user_db, @pss_db, @hang_db = dbs.map( &loader )
    end

    def select_location where
      case where
       when USERS_DB  then @user_db
       when PASSWORDS then @pss_db
       when HANGARDS  then @hang_db
      end
    end
    def rewrite_db! where
      witch = select_location where
      open( where, "w" ) { |db|
        db.write YAML::dump( witch )
      }
    end

    def add_to_db user_data, where
      from =  select_location(where)
      from.update user_data
      rewrite_db! where
    end

    def add_to_user_db user_data
      add_to_db user_data, USERS_DB
    end

    def add_to_password user_data
      add_to_db user_data, PASSWORDS
    end

    def add_to_hangard user_data
      add_to_db user_data, HANGARDS
    end

    def del_from_db name, from
      from.delete name
      rewrite_db! HANGARDS
    end

    def del_from_db_by_id id
      del_from_db id, @hang_db
    end

    def is_known? user_and_password
      user, pswd = user_and_password.to_a.shift
      !@pss_db[user].nil? and @pss_db[user] == pswd
    end
  end
end
