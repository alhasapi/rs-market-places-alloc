require "./DB.rb"
module Market
  Formater = "|  %s  |   %s   |  %s  |"
  class Agent
    attr_accessor :nom, :prenom, :pswd
    def initialize nom, prenom, pswd
      @nom, @prenom, @pswd = nom, prenom, pswd
      @hangards = (0..Market::HANGARD_MAX).map { |id| Hangard.new id }
    end

    def prelever client
      raise NotImplementedError
    end

    def to_hash
      { @nom => @pswd }
    end
  end

  class Responsable < Agent
    def initialize nom, prenom, pswd, db
      super(nom, prenom, pswd)
      @db  = db
      load_items_from_db
    end

    def allouer taille, client
      hangard = @hangards.select(&:vide?).sample
      hangard.taille  = taille
      hangard.proprio = client

      enregistrer hangard
      puts "[ ! ] allocation termine avec succes: hangard numero #{hangard.numero} pour #{client.nom}"
    end

    def load_items_from_db
      h_data, ids = @db.hang_db, @db.hang_db.keys
      assign_attr = ->( i ) {
        @hangards[i].taille  = h_data[i].first
        @hangards[i].proprio.nom = h_data[i].proprio_name
      }
      ids.each(&assign_attr)
    end
    def enregistrer hangard
      client_data = hangard.proprio.to_hash
      @db.add_to_user_db client_data
      @db.add_to_hangard hangard.to_hash
    end

    def supprimer_by client
      @db.del_from_db name, @db.user_db
      predicate ->( x, z ) { z.last == client.to_hash }
      id = @db.hang_db
           .select(&predicate)
           .shift
      supprimer id
    end

    def supprimer id
      name = @db.hang_db[id].proprio_name
      @db.del_from_db_by_id id
      puts "Le hangard de #{name} a ete desallour avec succes."
    end

    def allocation_pour nom_du_commercant, frais
        commercant = Market::Commercant.new nom_du_commercant, frais
        allouer (0..100).to_a.sample, commercant
    end

    def afficher_hangard_by predicat  
      data_str   = ->( item ) { Formater %[item.numero, item.taille, item.proprio.nom == "" ? "AUCUN" : item.proprio] }
      puts "|  ID   |   TAILLE   |  OCCUPANT |"
      puts "\\______|____________|___________/"
      puts @hangards.select(&predicat).collect(&data_str).join("\n")
    end

    def afficher_hangard_vide
      afficher_hangard_by :vide?
    end

    def afficher_deja_allouer
      afficher_hangard_by :occupe?
    end
  end

  class Commercant
    attr_accessor :nom, :compte
    def initialize nom, compte
      @nom, @compte = nom, compte
    end
    def to_s
      "#{nom} #{compte}"
    end

    def to_hash
      {@nom => @compte}
    end
  end

  class Hangard
    attr_accessor :numero, :taille, :proprio
    def initialize numero
      @numero  = numero
      @taille  = 0
      @proprio = Commercant.new("", 0)
    end

    def vide?
      @taille == 0
    end

    def occupe?
      !vide?
    end
    def to_hash
      {@numero => [@taille, @proprio.to_hash]}
    end
  end
end

class String
  def is_yes?
    self.downcase == "o"
  end
end

class Array
  def proprio_name
    self.last.keys.first
  end
end
# $cl = Market::Commercant.new("Al Ghazali", "Abu Hamid", 1000_000_090)
# $q  = Market::Responsable.new "Ibn Rush", "Muhammad", "tahafut", Market::DB.new

# def test
#   $q.allouer 200, $cl
# end
